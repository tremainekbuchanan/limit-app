package com.eightsevensix.limit;

import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.io.File;
import java.util.List;


public class Share extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share);
    }

    private void initShareIntent(String type) {
        boolean found = false;
        Intent share = new Intent(android.content.Intent.ACTION_SEND);
        share.setType("text/plain");

        // gets the list of intents that can be loaded.
        List<ResolveInfo> resInfo = getPackageManager().queryIntentActivities(share, 0);
        if (!resInfo.isEmpty()){
            for (ResolveInfo info : resInfo) {
                if (info.activityInfo.packageName.toLowerCase().contains(type) ||
                        info.activityInfo.name.toLowerCase().contains(type) ) {
                         share.setPackage(info.activityInfo.packageName);
                    share.putExtra(Intent.EXTRA_TEXT, "Get Limit today. Download here http://t.co/ELK86CYAOV");
                    found = true;
                    break;
                }
            }
            if (!found)
                return;

            startActivity(Intent.createChooser(share, "Select"));
        }
    }

   public void shareContent(View view){

       switch (view.getId()){
           case R.id.shareFb:
               initShareIntent("face");
               break;
           case R.id.shareTw:
               initShareIntent("twit");
               break;
           case R.id.shareWa:
               initShareIntent("whats");
               break;
       }
//        Intent intent = new Intent();
//        intent.setAction(Intent.ACTION_SEND);
//        intent.putExtra(Intent.EXTRA_TEXT, "This is sample text");
//        intent.setType("text/plain");
//        startActivity(Intent.createChooser(intent,"Share to"));


    }
}
