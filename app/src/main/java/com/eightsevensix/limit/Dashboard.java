package com.eightsevensix.limit;

import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.graphics.Typeface;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.util.List;

public class Dashboard extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_dashboard);
    }

    private void initShareIntent(String type) {
        boolean found = false;
        Intent share = new Intent(android.content.Intent.ACTION_SEND);
        share.setType("text/plain");

        // gets the list of intents that can be loaded.
        List<ResolveInfo> resInfo = getPackageManager().queryIntentActivities(share, 0);
        if (!resInfo.isEmpty()){
            for (ResolveInfo info : resInfo) {
                if (info.activityInfo.packageName.toLowerCase().contains(type) ||
                        info.activityInfo.name.toLowerCase().contains(type) ) {
                    share.setPackage(info.activityInfo.packageName);
                    if(type.equalsIgnoreCase("com.facebook.katana"))
                    {
                        share.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.share_url_fb));
                    }
                    else
                    {
                       if(type.equalsIgnoreCase("twit"))
                           share.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.share_url_twitter));
                    }
                    found = true;
                    break;
                }
            }
            if (!found)
                return;

            startActivity(Intent.createChooser(share, "Select"));
        }
    }

    public void shareContent(View view) {

        switch (view.getId()) {
            case R.id.shareFb:
                initShareIntent("com.facebook.katana");
                break;
            case R.id.shareTw:
                initShareIntent("twit");
                break;
            case R.id.shareWa:
                initShareIntent("whats");
                break;
        }
    }


}
