package com.eightsevensix.limit;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.telephony.SmsMessage;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import android.os.Vibrator;

public class SMSHandler extends BroadcastReceiver {

    private Object[] messages;
    private SmsMessage[] sms;
    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();

        messages = (Object[])bundle.get("pdus");
        sms = new SmsMessage[messages.length];
        for(int n=0;n <messages.length;n++){
            sms[n] = SmsMessage.createFromPdu((byte[]) messages[n]);
        }
        for(SmsMessage msg : sms){
            routeSMS(msg.getMessageBody().toLowerCase(), context);
        }
    }
    private int determineMessageType(String sms){
        int messageType = -1;
        boolean suff_credit  = sms.matches("(?i).*sorry you do not have sufficient.*");
        boolean data_plan = sms.matches ("(?i).*you have used all of your data plan allowance.*");
        if(suff_credit || data_plan)
            messageType = 1;

       return messageType;
    }
    private void routeSMS(String sms, Context context){
         switch (determineMessageType(sms)){
            case 1:
                showNotification(context, "Your mobile data has been turned off. ", "No mobile data left");
                setMobileDataEnabled(context, false);
                vibrateDevice(context, 1000);
             default:
                //Toast.makeText(context, "Error", Toast.LENGTH_LONG).show();
                break;
        }

    }
    private void showNotification(Context context, String contentText, String contentTitle) {
        Intent intent = new Intent(context, Share.class);
        PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(
                context)
                // Set Icon
                .setSmallIcon(R.drawable.ic_launcher)
                        // Set Ticker Message
                .setTicker(contentTitle)
                        // Set Title
                .setContentTitle(contentTitle)
                        // Set Text
                .setContentText(contentText)
                .setContentIntent(pIntent)
                        // Dismiss Notification
                .setAutoCancel(true);
       NotificationManager notificationmanager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        notificationmanager.notify(0, builder.build());
    }

    private void setMobileDataEnabled(Context context, boolean enabled) {
        final ConnectivityManager conman =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        try {
            final Class conmanClass = Class.forName(conman.getClass().getName());
            final Field iConnectivityManagerField = conmanClass.getDeclaredField("mService");
            iConnectivityManagerField.setAccessible(true);
            final Object iConnectivityManager = iConnectivityManagerField.get(conman);
            final Class iConnectivityManagerClass = Class.forName(
                    iConnectivityManager.getClass().getName());
            final Method setMobileDataEnabledMethod = iConnectivityManagerClass
                    .getDeclaredMethod("setMobileDataEnabled", Boolean.TYPE);
            setMobileDataEnabledMethod.setAccessible(true);
            setMobileDataEnabledMethod.invoke(iConnectivityManager, enabled);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
    }

    private void vibrateDevice(Context context, long milliseconds){
        Vibrator v = (Vibrator)context.getSystemService(Context.VIBRATOR_SERVICE);
        if(v.hasVibrator()){
            v.vibrate(milliseconds);
        }
    }
}

